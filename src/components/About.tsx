import {
  faEnvelope,
  faMapMarker,
  faPhone,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-scroll';

import config from '../config/index.json';

const About = () => {
  const { about, navigation } = config;

  return (
    <div
      id="about"
      className="container mx-auto px-4 py-12 flex flex justify-center align-start left-padding"
    >
      <div className="mb-4 mb-md-0 full-width">
        <p className="mt-2 text-2xl leading-1 font-bold tracking-tight text-gray-900 sm:text-l md:text-1xl lg:text-2xl">
          Ügyfélszolgálat:
        </p>
        <ul>
          <li>Hétfő: 10:00-14:00</li>
          <li>Kedd: 10:00-14:00</li>
          <li>Szerda: 10:00-14:00</li>
          <li>Csütörtök: 10:00-14:00</li>
          <li>Péntek: 10:00-14:00</li>
          <li>Szombat: Zárva</li>
          <li>Vasárnap: Zárva</li>
        </ul>
      </div>
      <div className="mb-4 mb-md-0 full-width">
        <p className="mt-2 text-2xl leading-1 font-bold tracking-tight text-gray-900 sm:text-l md:text-1xl lg:text-2xl">
          Oldaltérkép:
        </p>
        <ul>
          {navigation.map((item) => (
            <li key={item.name}>
              <Link
                spy={true}
                active="active"
                smooth={true}
                duration={1000}
                to={item.href}
                className="font-medium text-gray-500 hover:text-gray-900"
              >
                {item.name}
              </Link>
            </li>
          ))}
        </ul>
      </div>
      <div className="mb-4 mb-md-0 full-width">
        <p className="mt-2 text-2xl leading-1 font-bold tracking-tight text-gray-900 sm:text-l md:text-1xl lg:text-2xl">
          Elérhetőségek:
        </p>
        <a
          href="https://goo.gl/maps/4rCjp3XRPw1Jgn4o6"
          target="_blank"
          rel="noreferrer"
        >
          <p>
            <span className="elementor-icon-list-icon">
              <FontAwesomeIcon icon={faMapMarker} />
            </span>
            <span className="elementor-icon-list-text"> {about.address}</span>
          </p>
        </a>
        <a href={`tel:${about.phone}`}>
          <p>
            <span className="elementor-icon-list-icon">
              <FontAwesomeIcon icon={faPhone} />
            </span>
            <span className="elementor-icon-list-text"> {about.phone}</span>
          </p>
        </a>
        <a href={`mailto:${about.email}`}>
          <p>
            <span className="elementor-icon-list-icon">
              <FontAwesomeIcon icon={faEnvelope} />
            </span>
            <span className="elementor-icon-list-text"> {about.email}</span>
          </p>
        </a>
      </div>
    </div>
  );
};
export default About;
