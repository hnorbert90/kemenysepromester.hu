import * as XLSX from 'xlsx';

import { SheetData } from './SheetData';

function readExcelFile(): Promise<SheetData[]> {
  return fetch('/assets/xls/prices.xlsx')
    .then((x) => x.arrayBuffer())
    .then((b) => {
      const workbook = XLSX.read(new Uint8Array(b), { type: 'array' });

      const sheetsData: SheetData[] = [];

      workbook.SheetNames.forEach((sheetName) => {
        const worksheet = workbook.Sheets[sheetName] as any;

        const rows = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
        const columns = (rows.shift() as any) || {};

        const sheetData: SheetData = {
          sheetName,
          columns: Object.values(columns),
          rows: rows.map((x: any) => Object.values(x)),
        };

        sheetsData.push(sheetData);
      });
      return sheetsData;
    });
}

export default readExcelFile;
