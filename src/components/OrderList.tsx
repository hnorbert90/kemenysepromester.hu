import React from 'react';

import AddCircleIcon from '@mui/icons-material/AddCircle';
import SendIcon from '@mui/icons-material/Send';
import { Button, Grid, TextField } from '@mui/material';

import OrderItem from './OrderItem';

const OrderList = (props: any) => {
  const { orderItems, setOrderItems, defaultValue } = props;
  const products = props.products.map((row: any) => {
    return {
      productName: row[0],
      price: row[1],
      unit: row[2],
      quantity: 0,
    };
  });

  const rowPrice = (product: any) => {
    let result = 0;
    if (
      product &&
      typeof product.quantity === 'number' &&
      typeof product.price === 'number'
    ) {
      const val = product.quantity * product.price;
      // eslint-disable-next-line no-restricted-globals
      result = isNaN(val) ? 0 : val;
    }
    return result; // or some other appropriate value
  };
  const finalPrice = () => {
    return orderItems.reduce((sum: any, item: any) => sum + rowPrice(item), 0);
  };
  const emailBody = () => {
    let result =
      'Tisztelt Címzett!%0d%0a%0d%0aSzeretnék ajánlatot kérni az alábbi tételekre:%0d%0a';
    orderItems.forEach((item: any) => {
      result += `%20%20%20%20-${item.quantity} ${item.unit.split('/')[1]} ${
        item.productName
      } 
      %0d%0a`;
    });
    result += `%0d%0aKalkulált végösszeg: ${finalPrice().toLocaleString(
      'hu-HU',
      {
        style: 'currency',
        currency: 'HUF',
      }
    )}`;
    return result;
  };

  return (
    <Grid container spacing={4}>
      {orderItems.map((oi: any, i: number) => (
        <Grid key={i} item xs={12}>
          <OrderItem
            products={products}
            product={oi}
            rowPrice={rowPrice}
            defaultValue={defaultValue}
            handleDelete={(id: number) => {
              setOrderItems(orderItems.filter((x: any) => x.id !== id));
            }}
            onProductChange={(product: any, id: number) => {
              if (!product) {
                const clone = [...orderItems];
                const orderItem = clone.find((x) => x.id === id);
                if (orderItem) {
                  Object.assign(orderItem, { ...defaultValue, id });
                  setOrderItems(clone);
                }
              } else {
                const clone = [...orderItems];
                const orderItem = clone.find((x) => x.id === id);
                if (orderItem) {
                  Object.assign(orderItem, { ...product, id });
                  setOrderItems(clone);
                }
              }
            }}
          />
        </Grid>
      ))}
      <Grid item xs={12} sm={8}>
        <Button
          variant="contained"
          color="error"
          endIcon={<AddCircleIcon />}
          onClick={() => {
            setOrderItems((oi: any) => [
              ...oi,
              { ...defaultValue, id: oi.length + 1 },
            ]);
          }}
        >
          Hozzáad
        </Button>
      </Grid>
      <Grid item xs={10} sm={3} textAlign={'left'}>
        <TextField
          disabled
          id="endPrice"
          label="Végösszeg"
          defaultValue="0"
          value={finalPrice().toLocaleString('hu-HU', {
            style: 'currency',
            currency: 'HUF',
          })}
          variant="filled"
          fullWidth
        />
      </Grid>
      <Grid item xs={10} sm={11} textAlign="right">
        <Button
          id="emailButton"
          className="button-width"
          variant="contained"
          color="error"
          disabled={finalPrice() === 0}
          endIcon={<SendIcon />}
          onClick={() => {
            window.open(
              `mailto:kemenyellenor.info@gmail.com?subject=Egyedi ajánlat kérése&body=${emailBody()}`
            );
          }}
        >
          Ajánlat kérés
        </Button>
      </Grid>
    </Grid>
  );
};
export default OrderList;
