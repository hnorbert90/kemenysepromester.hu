export interface SheetData {
  sheetName: string;
  columns: string[];
  rows: any[];
}
