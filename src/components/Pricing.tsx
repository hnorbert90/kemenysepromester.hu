import React, { useEffect, useState } from 'react';

import { Box, Typography } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';

import config from '../config/index.json';
import readExcelFile from './excelService';
import OrderList from './OrderList';
import PricingTable from './PriceTable';
import { SheetData } from './SheetData';

const Pricing = (props: any) => {
  const { priceView, setPriceView } = props;
  const defaultValue = {
    productName: 'Válasszon égéstermék-elvezetőt',
    price: 0,
    unit: '',
    quantity: 0,
    id: 0,
  };
  const [pricingTables, setPricingTables] = useState([]);
  const [orderItems, setOrderItems] = useState<any[]>([{ ...defaultValue }]);

  const { pricing } = config;
  const { title, subTitle } = pricing;

  useEffect(() => {
    readExcelFile().then((x: any) => setPricingTables(x));
  }, []);

  return (
    <section className={`bg-background py-8`} id="pricing">
      <div className={`container mx-auto px-2 pt-4 pb-12 text-primary`}>
        <h1
          className={`w-full my-2 text-5xl font-bold leading-tight text-center text-primary`}
        >
          {title}
        </h1>
        <h5
          className={`w-full font-bold leading-tight text-center text-secondary`}
        >
          {subTitle}
        </h5>
        <div>
          {pricingTables?.map((sheet: SheetData, i: number) => (
            <Box key={i} className="p-5">
              <Box className="w-full" textAlign={'center'}>
                <FormControl>
                  <RadioGroup
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="row-radio-buttons-group"
                    value={priceView}
                    onChange={(e: any) => setPriceView(e.target.value)}
                  >
                    <FormControlLabel
                      value="list"
                      control={<Radio />}
                      label={
                        <Typography color="textPrimary">Árlista</Typography>
                      }
                    />
                    <FormControlLabel
                      value="calc"
                      color="secondary"
                      control={<Radio />}
                      label={
                        <Typography color="textPrimary">Kalkulátor</Typography>
                      }
                    />
                  </RadioGroup>
                </FormControl>
              </Box>
              <Box p={5}>
                {priceView === 'list' && <PricingTable sheetData={sheet} />}
                {priceView === 'calc' && (
                  <OrderList
                    orderItems={orderItems}
                    setOrderItems={setOrderItems}
                    defaultValue={defaultValue}
                    products={sheet.rows}
                  />
                )}
              </Box>
            </Box>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Pricing;
