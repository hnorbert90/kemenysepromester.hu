import React from 'react';

import { Delete } from '@mui/icons-material';
import {
  Autocomplete,
  InputAdornment,
  TextField,
  IconButton,
} from '@mui/material';
import Grid from '@mui/material/Grid';

interface OrderItemProps {
  products: any[];
  product: any;
  defaultValue: any;
  onProductChange: (product: any, id: number) => void;
  handleDelete: (id: number) => void;
  rowPrice: (product: any) => any;
}

const OrderItem = (props: OrderItemProps) => {
  const {
    products,
    product,
    defaultValue,
    onProductChange,
    handleDelete,
    rowPrice,
  } = props;

  const unit = () => {
    if (product && product.unit) {
      const splitUnit = product.unit.split('/');
      return splitUnit.length > 1 ? splitUnit[1] : '';
    }
    return '';
  };

  return (
    <Grid container spacing={4} alignItems="center">
      <Grid item xs={10} sm={5}>
        <Autocomplete
          disablePortal
          id="productName"
          options={products}
          value={product}
          getOptionLabel={(option) => option.productName}
          onChange={(_event: any, newValue: any) => {
            onProductChange(newValue, product.id);
          }}
          sx={{ width: '100%' }}
          renderInput={(params) => (
            <TextField {...params} label="Égéstermék-elvezető" />
          )}
        />
      </Grid>
      <Grid item xs={10} sm={3}>
        <TextField
          id="quantity"
          label="Adja meg a mennyiséget"
          type="number"
          defaultValue="0"
          disabled={product.productName === defaultValue.productName}
          value={product.quantity}
          sx={{ width: '100%' }}
          onChange={(
            e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
          ) => {
            const value = parseInt(e.target.value, 10);
            onProductChange({ ...product, quantity: value }, product.id);
          }}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">{unit()}</InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid item xs={10} sm={3}>
        <TextField
          disabled
          id="endPrice"
          label="Összeg"
          defaultValue="0"
          value={rowPrice(product).toLocaleString('hu-HU', {
            style: 'currency',
            currency: 'HUF',
          })}
          variant="filled"
          sx={{ width: '100%' }}
        />
      </Grid>
      <Grid item xs={2} sm={1} textAlign={'center'}>
        <IconButton
          aria-label="delete"
          onClick={() => handleDelete(product.id)}
        >
          <Delete />
        </IconButton>
      </Grid>
    </Grid>
  );
};
export default OrderItem;
