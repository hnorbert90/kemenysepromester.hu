import React from 'react';

import { SheetData } from './SheetData';

interface Props {
  sheetData: SheetData;
}

const PricingTable: React.FC<Props> = ({ sheetData }) => {
  if (!sheetData) {
    return null;
  }

  const format = (cell: any) => {
    if (typeof cell === 'number') {
      return cell.toLocaleString('hu-HU', {
        style: 'currency',
        currency: 'HUF',
      });
    }

    return cell;
  };

  return (
    <>
      <table className="alternate-background">
        <thead>
          <tr>
            {sheetData.columns.map((column) => (
              <th key={column}>{column}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {sheetData.rows.map((row, index) => (
            <tr
              key={index}
              className={index % 2 === 0 ? 'table-row-even' : 'table-row-odd'}
            >
              {row.map((cell: any, cellIndex: any) => (
                <td key={cellIndex}>{format(cell)}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      <div className="p-10"></div>
      <table>
        <tbody>
          <tr className="table-row-even">
            {
              <>
                <td>Kiszállási díj:</td>
                <td>
                  {(5100).toLocaleString('hu-HU', {
                    style: 'currency',
                    currency: 'HUF',
                  })}
                </td>
              </>
            }
          </tr>
        </tbody>
      </table>
    </>
  );
};

export default PricingTable;
