import React, { useState } from 'react';

import About from '../components/About';
import Header from '../components/Header';
import LandingPage from '../components/LandingPage';
import LandingPageImage from '../components/LandingPageImage';
import LazyShow from '../components/LazyShow';
import Pricing from '../components/Pricing';
import Services from '../components/Services';

const App = () => {
  const [priceView, setPriceView] = useState('list');

  return (
    <div className={`bg-background grid gap-y-16 overflow-hidden`}>
      <div className={`relative bg-background`}>
        <div className="max-w-7xl mx-auto">
          <div
            className={`relative z-10 pb-8 bg-background sm:pb-16 md:pb-20 lg:max-w-2xl lg:w-full lg:pb-28 xl:pb-32`}
          >
            <Header />
            <LandingPage setPriceView={setPriceView} />
          </div>
        </div>
        <LandingPageImage />
      </div>
      <LazyShow>
        <>
          <Services />
        </>
      </LazyShow>
      <LazyShow>
        <Pricing setPriceView={setPriceView} priceView={priceView} />
      </LazyShow>
      <LazyShow>
        <>
          <About />
        </>
      </LazyShow>
    </div>
  );
};

export default App;
